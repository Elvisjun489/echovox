<?php


namespace App\Controller;


use App\Entity\Feedback;
use App\Form\FeedbackType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FeedbackController extends AbstractController
{
    /**
     * @param Request $request
     * @Route("/", name="new_feedback")
     * @return Response
     */
    public function newFeedbackAction(Request $request)
    {
        $feedback = new Feedback();

        $form = $this->createForm(FeedbackType::class, $feedback);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($feedback);
            $em->flush();

            $this->addFlash('success', 'thank you for your message');

            return $this->redirectToRoute('new_feedback');
        }

        return $this->render('form.html.twig', [
            'form' => $form->createView()
        ]);
    }

}