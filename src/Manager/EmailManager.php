<?php


namespace App\Manager;


use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\FrameworkBundle\Templating\PhpEngine;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EmailManager
{
    private $mailer;

    private $engine;

    private $adminEmail;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $engine, string $adminEmail)
    {
        $this->mailer = $mailer;
        $this->adminEmail = $adminEmail;
        $this->engine = $engine;
    }

    public function send(EmailData $emailData)
    {
        try {
            $message = (new \Swift_Message($emailData->getSubject()))
                ->setTo($this->adminEmail)
                ->setFrom($this->adminEmail)
                ->setBody(
                    $this->engine->renderResponse($emailData->getView(), $emailData->getTemplateParameters()),
                    'text/html'
                );


            $this->mailer->send($message);
        } catch (\Throwable $e) {
            throw new HttpException(500, $e->getMessage());
        }
    }

}