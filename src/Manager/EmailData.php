<?php


namespace App\Manager;


class EmailData
{
    private $subject;

    private $view;

    private $templateParameters;

    public function __construct($subject, string $view, array $templateParameters)
    {

        $this->subject = $subject;

        $this->view = $view;

        $this->templateParameters = $templateParameters;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getTemplateParameters()
    {
        return $this->templateParameters;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

}