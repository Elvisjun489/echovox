<?php


namespace App\EntityListeners;


use App\Entity\Feedback;
use App\Manager\EmailData;
use App\Manager\EmailManager;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

class FeedbackListener
{
    private $emailManager;

    public function __construct(EmailManager $emailManager)
    {
        $this->emailManager = $emailManager;
    }

    /**
     * @param Feedback $feedback
     * @param LifecycleEventArgs $args
     * @ORM\PostPersist()
     */
    public function postPersistHandler(Feedback $feedback, LifecycleEventArgs $args)
    {
        $emailData = new EmailData(
            'New Feedback',
                'feedback_email.html.twig',
            [
                'feedback' => $feedback
            ]

        );

        $this->emailManager->send($emailData);
    }
}