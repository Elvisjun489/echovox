### Steps for install

- docker-compose build
- docker-compose up -d
- docker-compose exec php bash

####Run from container:
- composer install
- yarn install
- yarn encore dev (or prod)
- bin/console d:m:m


I'm used port :82 for application and :8033 for phpmyadmin (was free on my local machine)

###Email settings
for email you need add MAILER_URL into .env file and system_email in parameters (messages will sent from this one)